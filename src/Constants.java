public class Constants {
    public enum Direction {UP, DOWN, LEFT, RIGHT, NONE}
    public static final double WIN_MARGIN = 0.1;
}
