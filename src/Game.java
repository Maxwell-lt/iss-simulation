import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Game logic class
 *
 * When instantiated, this creates a window and begins the game.
 */
public class Game {
    private static final Logger LOGGER = Logger.getLogger(Game.class.getName());
    private Tracker tracker;
    private Shuttle shuttle;
    private JFrame window;
    private JPanel buttonPane;
    private JButton[] controls;
    private JTextArea info;
    private JButton save;
    private ImageDisplay graphicView;
    private JButton reset;

    // TODO: Constructor that accepts a Tracker, and plays back the steps instead of allowing user input.

    public Game() {
        long seed = System.currentTimeMillis();
        shuttle = new Shuttle(50, 15, 100, 250, seed);
        tracker = new Tracker(seed);
        shuttle.setTracker(tracker);

        window = new JFrame("ISS Docking Simulation");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setLayout(new GridBagLayout());

        createImagePanel();
        createButtonPanel();
        window.getContentPane().add(buttonPane, getConstraints(1, 2));


        window.pack();
        window.setVisible(true);
    }

    private void reset() {
        long seed = System.currentTimeMillis();
        shuttle = new Shuttle(50, 15, 100, 250, seed);
        tracker = new Tracker(seed);
        shuttle.setTracker(tracker);

        window.getContentPane().removeAll();

        createImagePanel();
        createButtonPanel();
        window.getContentPane().add(buttonPane, getConstraints(1, 2));


        window.pack();
        window.setVisible(true);
    }

    private void createImagePanel() {
        graphicView = new ImageDisplay(shuttle);
        window.add(graphicView, getConstraints(1, 1));
    }

    private void createButtonPanel() {
        buttonPane = new JPanel();
        buttonPane.setLayout(new GridBagLayout());

        JButton up = new JButton();
        JButton down = new JButton();
        JButton left = new JButton();
        JButton right = new JButton();
        JButton none = new JButton();
        JButton coast = new JButton();
        info = new JTextArea();
        save = new JButton();
        reset = new JButton();

        up.setText("Up");
        down.setText("Down");
        left.setText("Left");
        right.setText("Right");
        none.setText("Neutral");
        coast.setText("Coast");
        save.setText("Save...");
        reset.setText("New Game...");


        info.setEditable(false);
        save.setEnabled(false);
        reset.setEnabled(false);

        updateDisplays();

        controls = new JButton[] {up, down, left, right, none, coast};

        up.addActionListener(e -> {
            shuttle.update(Constants.Direction.UP);
            checkState();
            updateDisplays();
        });

        down.addActionListener(e -> {
            shuttle.update(Constants.Direction.DOWN);
            checkState();
            updateDisplays();
        });

        left.addActionListener(e -> {
            shuttle.update(Constants.Direction.LEFT);
            checkState();
            updateDisplays();
        });

        right.addActionListener(e -> {
            shuttle.update(Constants.Direction.RIGHT);
            checkState();
            updateDisplays();
        });

        none.addActionListener(e -> {
            shuttle.update(Constants.Direction.NONE);
            checkState();
            updateDisplays();
        });

        coast.addActionListener(e -> {
            for (int i = 5; i > 0; i--) {
                shuttle.update(Constants.Direction.NONE);
                if (checkState()) {
                    break;
                }
            }
            updateDisplays();
        });

        save.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter(new FileNameExtensionFilter("ISS Simulator Save Files", "txt"));
            if (chooser.showSaveDialog(window) == JFileChooser.APPROVE_OPTION) {
                saveData(chooser.getSelectedFile());
            }
        });

        reset.addActionListener(e -> reset());

        buttonPane.add(up, getConstraints(3, 1));
        buttonPane.add(down, getConstraints(3, 3));
        buttonPane.add(left, getConstraints(2, 2));
        buttonPane.add(right, getConstraints(4, 2));
        buttonPane.add(none, getConstraints(3, 2));
        //buttonPane.add(info, getConstraints(1, 1, 3, 1));
        buttonPane.add(coast, getConstraints(4, 3));
        buttonPane.add(save, getConstraints(5, 1, 3, 1));
        buttonPane.add(reset, getConstraints(1, 2));

        window.getContentPane().add(buttonPane, getConstraints(1, 2));
    }

    private void saveData(File selectedFile) {
        String content = tracker.save();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(selectedFile))) {
            bw.write(content);
            LOGGER.log(Level.INFO, "Successfully saved game replay to {0}", selectedFile.getName());
        }
        catch (IOException e) {
            LOGGER.log(Level.WARNING, "Failed to save game replay to {0}", selectedFile.getName());
            JOptionPane.showMessageDialog(window, "Unable to write to selected file", "Game failed to save", JOptionPane.ERROR_MESSAGE);
        }
    }

    private GridBagConstraints getConstraints(int gridx, int gridy) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = gridx;
        c.gridy = gridy;
        c.insets = new Insets(5, 5, 5, 5);

        return c;
    }

    private GridBagConstraints getConstraints(int gridx, int gridy, int gridheight, int gridwidth) {
        GridBagConstraints c = getConstraints(gridx, gridy);
        c.gridheight = gridheight;
        c.gridwidth = gridwidth;

        return c;
    }

    private void updateDisplays() {
        if (shuttle.checkForVictory()) {
            graphicView.setVictory();
        } else if (shuttle.checkForDefeat()) {
            graphicView.setDefeat();
        }
        graphicView.repaint();
    }

    private boolean checkState() {
        boolean gameEnd = false;
        if (shuttle.checkForVictory()) {
            LOGGER.log(Level.INFO, "Successfully docked with ISS after {0} turns.", shuttle.getAge());
            JOptionPane.showMessageDialog(window, "Successfully docked with the ISS!",
                    "Victory", JOptionPane.INFORMATION_MESSAGE);
            gameEnd = true;
        } else if (shuttle.checkForDefeat()) {
            LOGGER.log(Level.INFO, "Failed to dock with ISS, crashed after {0} turns.", shuttle.getAge());
            JOptionPane.showMessageDialog(window, "Oops, you crashed the shuttle",
                    "Failure", JOptionPane.ERROR_MESSAGE);
            gameEnd = true;
        }
        if (gameEnd) {
            for(JButton b : controls) {
                b.setEnabled(false);
            }
            save.setEnabled(true);
            reset.setEnabled(true);
        }
        return gameEnd;

    }
}
