import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Shuttle
 *
 * Provides a shuttle object that manages its position and velocity relative to the ISS
 */
public class Shuttle {
    private static final Logger LOGGER = Logger.getLogger(Shuttle.class.getName());
    private Random random;
    private double xOffset, yOffset;
    private double xVelocity, yVelocity;
    private double distance;
    private int age;
    private Tracker tracker;
    /**
     * Units are m/s^2
     */
    private final double ACCELERATION = 1;
    /**
     * Approach speed of shuttle.
     */
    private final double Z_VELOCITY = 1; // Should be 0.0325, changed for testing

    /**
     * Constructor for the shuttle.
     * @param maxX Maximum distance from the origin in the X axis in meters.
     * @param maxY Maximum distance from the origin in the Y axis in meters.
     * @param minDist Minimum distance from shuttle to ISS.
     * @param maxDist Maximum distance from shuttle to ISS.
     * @param seed Seed for the random generator
     */
    public Shuttle(int maxX, int maxY, int minDist, int maxDist, long seed) {
        random = new Random(seed);
        xOffset = (random.nextInt(maxX * 2) - maxX);
        yOffset = (random.nextInt(maxY * 2) - maxY);
        distance = (random.nextInt(maxDist - minDist) + minDist);
    }

    /**
     * Update method, runs one simulation step given an input.
     *
     * Each simulation step is one second.
     * @param direction Which way the shuttle should accelerate
     */
    public void update(Constants.Direction direction) {
        LOGGER.log(Level.INFO, "Received direction input {0}", direction.name());
        incrementAge();
        if (tracker != null) {
            tracker.addStep(direction);
        }

        switch (direction) {
            case NONE:
                break;
            case UP:
                yVelocity += ACCELERATION;
                break;
            case DOWN:
                yVelocity -= ACCELERATION;
                break;
            case RIGHT:
                xVelocity += ACCELERATION;
                break;
            case LEFT:
                xVelocity -= ACCELERATION;
                break;
        }

        xOffset += xVelocity;
        yOffset += yVelocity;

        distance -= Z_VELOCITY;
    }

    public boolean checkForVictory() {
        return (Math.abs(xOffset) < Constants.WIN_MARGIN)
                && (Math.abs(yOffset) < Constants.WIN_MARGIN)
                && (distance < Constants.WIN_MARGIN)
                && (distance > 0 - Constants.WIN_MARGIN);
    }

    public boolean checkForDefeat() {
        return !checkForVictory() && (distance + Constants.WIN_MARGIN) < 0;
    }

    public double getxVelocity() {
        return xVelocity;
    }

    public double getyVelocity() {
        return yVelocity;
    }

    private void incrementAge() {
        age++;
    }

    /**
     * Gets the distance between the shuttle and the ISS in meters.
     * @return Distance in meters
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Gets the offset along the X axis from the center in meters.
     * @return Offset in meters from center
     */
    public double getxOffset() {
        return xOffset;
    }

    /**
     * Gets the offset along the Y axis from the center in meters.
     * @return Offset in meters from center
     */
    public double getyOffset() {
        return yOffset;
    }

    /**
     * Gets the number of simulation steps run so far.
     * @return Number of steps run
     */
    public int getAge() {
        return age;
    }

    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }
}
