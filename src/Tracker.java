import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Tracker {
    private static final Logger LOGGER = Logger.getLogger(Tracker.class.getName());
    private ArrayList<Constants.Direction> steps;
    private long seed;

    public Tracker(long seed) {
        this.seed = seed;
        steps = new ArrayList<>();
    }

    public void addStep(Constants.Direction direction) {
        steps.add(direction);
    }

    public String save() {
        StringBuilder builder = new StringBuilder();
        builder.append(seed);
        builder.append("\n");
        for (Constants.Direction direction : steps) {
            builder.append(direction.name());
            builder.append("\n");
        }
        return builder.toString();
    }

    public static Tracker load(String savedState) {
        List<String> lines = Arrays.asList(savedState.split("\\r?\\n"));
        long seed;
        try {
            seed = Long.parseLong(lines.get(0));
        } catch (NumberFormatException e) {
            LOGGER.log(Level.WARNING, e.toString(), e);
            return null;
        } finally {
            lines.remove(0);
        }

        Tracker tracker = new Tracker(seed);
        for (String s : lines) {
            try {
                tracker.addStep(Constants.Direction.valueOf(s));
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.WARNING, e.toString(), e);
                return null;
            }
        }
        LOGGER.log(Level.INFO, "Successfully reconstructed a Tracker");
        return tracker;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public Iterator<Constants.Direction> getSteps() {
        return steps.iterator();
    }
}
