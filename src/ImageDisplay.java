import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

class ImageDisplay extends JPanel {
    private static final Logger LOGGER = Logger.getLogger(ImageDisplay.class.getName());
    private final int WIDTH = 960;
    private final int HEIGHT = 640;
    private BufferedImage issImage;
    private BufferedImage background;
    private Shuttle shuttle;
    private boolean victory = false, defeat = false;

    public ImageDisplay(Shuttle shuttle) {
        super();
        this.shuttle = shuttle;
        issImage = loadImage("iss-render-25scale.png");
        background = loadImage("background.jpg");
        repaint();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(WIDTH, HEIGHT);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, WIDTH, HEIGHT, this);

        //drawISS(g);
        drawCapsule(g);
        drawVectors(g);
        drawHUD(g);

        if (victory) {
            displayVictory(g);
        } else if (defeat) {
            displayDefeat(g);
        }
    }

    private void drawISS(Graphics g) {
        final int pixelsPerMeter = 90;
        final int[] imageCenter = {4816, 1248};

        double aspectRatio = 1.5;

        int pixelScaleX = (int) shuttle.getDistance() * 11 + 760; // At the largest distance of 250m, this is in bounds if centered.
        int pixelScaleY = (int) (pixelScaleX / aspectRatio);

        int subCenterX = (int) (imageCenter[0] + shuttle.getxOffset() * pixelsPerMeter);
        int subCenterY = (int) (imageCenter[1] + shuttle.getyOffset() * pixelsPerMeter);

        int subCornerX = subCenterX - (pixelScaleX / 2);
        int subCornerY = subCenterY - (pixelScaleY / 2);

        LOGGER.log(Level.INFO, String.format("getWidth: %d, subCornerX: %d, subCornerX + pixelScaleX: %d", issImage.getWidth(), subCornerX, (subCornerX + pixelScaleX)));
        LOGGER.log(Level.INFO, "Attempting to access picture coords (" +
                Math.min(pixelScaleX, Math.max(issImage.getWidth() - (subCornerX + pixelScaleX), 0)) + "," + Math.min(pixelScaleY, Math.max(issImage.getHeight() - (subCornerY + pixelScaleY), 0)) + ")");
        BufferedImage subImage = issImage.getSubimage(
                Math.max(subCornerX, 0),
                Math.max(subCornerY, 0),
                Math.min(pixelScaleX, Math.max(issImage.getWidth() - (subCornerX + pixelScaleX), 1)),
                Math.min(pixelScaleY, Math.max(issImage.getHeight() - (subCornerY + pixelScaleY), 1)));

        boolean offLeft = false, offRight = false, offTop = false, offBottom = false;

        if (subImage.getWidth() != pixelScaleX) {
            if (subCornerX < 0) {
                offLeft = true;
            } else {
                offRight = true;
            }
        }

        if (subImage.getHeight() != pixelScaleY) {
            if (subCornerY < 0) {
                offTop = true;
            } else {
                offBottom = true;
            }
        }
        g.drawImage(subImage,
                offLeft ? WIDTH - (pixelScaleX - subImage.getWidth()) * (WIDTH/pixelScaleX) : 0,
                offBottom ? HEIGHT - (pixelScaleY - subImage.getHeight()) * (HEIGHT/pixelScaleY) : 0,
                offRight ? issImage.getWidth() * (WIDTH/pixelScaleX) : WIDTH,
                offTop ? issImage.getHeight() * (HEIGHT/pixelScaleY) : HEIGHT,
                this);
    }

    private void drawCapsule(Graphics g) {
        final int radius = 64;
        g.setColor(Color.BLACK);
        g.fillOval((WIDTH / 2) - radius, (HEIGHT / 2) - radius, radius * 2, radius * 2);

        g.setColor(Color.LIGHT_GRAY);
        g.fillOval((WIDTH / 2) - 40, (HEIGHT / 2) - 40, 20, 20);
        g.fillOval((WIDTH / 2) + 20, (HEIGHT / 2) - 40, 20, 20);
        g.fillOval((WIDTH / 2) - 40, (HEIGHT / 2) + 20, 20, 20);
        g.fillOval((WIDTH / 2) + 20, (HEIGHT / 2) + 20, 20, 20);
    }

    private void drawVectors(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int[] center = {WIDTH / 2, HEIGHT / 2};
        int[] velocity = {(WIDTH / 2) + (int) shuttle.getxVelocity() * 10, (HEIGHT / 2) - (int) shuttle.getyVelocity() * 10};
        int[] displacement = {(WIDTH / 2) + (int) shuttle.getxOffset() * 10, (HEIGHT / 2) - (int) shuttle.getyOffset() * 10};
        g2.setStroke(new BasicStroke(3));
        g2.setColor(Color.RED);
        g2.draw(new Line2D.Float(center[0], center[1], velocity[0], velocity[1]));
        g2.setColor(Color.ORANGE);
        g2.draw(new Line2D.Float(center[0], center[1], displacement[0], displacement[1]));
    }

    private void drawHUD(Graphics g) {
        g.setFont(g.getFont().deriveFont(14F));
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, 600, 80);
        g.setColor(Color.BLACK);
        g.drawString(String.format("Distance to ISS: %.2f meters", shuttle.getDistance()), 5, 10);
        g.drawString(String.format("Current Velocity: (%.2fx + %.2fy) m/s", shuttle.getxVelocity(), shuttle.getyVelocity()), 5, 25);
        g.drawString(String.format("Current Displacement: (%.2fx + %.2fy) meters", shuttle.getxOffset(), shuttle.getyOffset()), 5, 40);
        g.setColor(Color.RED);
        g.drawString("Velocity", 5, 55);
        g.setColor(Color.ORANGE);
        g.drawString("Displacement", 5, 70);
    }

    private void displayVictory(Graphics g) {
        g.setColor(Color.GREEN);
        g.setFont(g.getFont().deriveFont(100F));
        g.drawString("Victory!", 15, 200);
    }

    private void displayDefeat(Graphics g) {
        g.setColor(Color.RED);
        g.setFont(g.getFont().deriveFont(100F));
        g.drawString("Defeat.", 15, 200);
    }

    private BufferedImage loadImage(String filePath) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(getClass().getResource(filePath));
            LOGGER.log(Level.INFO, "Successfully loaded file {0}", filePath);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to load file {0}", filePath);
        }
        return image;
    }

    public void setVictory() {
        victory = true;
    }

    public void setDefeat() {
        defeat = true;
    }
}